# ninja wheel for macOS

Build [`ninja`](https://github.com/scikit-build/ninja-python-distributions) wheels suitable for macOS' system Python.

## License

[GPL-2.0-or-later](LICENSE)

